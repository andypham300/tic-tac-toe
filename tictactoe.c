#include <stdio.h>

const int rows = 3;
const int cols = 3;

void printBoard(char board[rows][cols]) {
	printf("\n");
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			printf("%c", board[i][j]);
		}

		printf("\n");
	}
	printf("\n");
}

int checkWin(char board[rows][cols]) {
	char x = 'X';
	char o = 'O';

	int xCount = 0;
	int oCount = 0;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			if (board[i][j] == x) {
				xCount += 1;
			}

			if (board[i][j] == o) {
				oCount += 1;
			}
		}
		if (xCount == 3) {
			return 0;
		}

		if (oCount == 3) {
			return 0;
		}

		xCount = 0;
		oCount = 0;
	}

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			if (board[j][i] == x) {
				xCount += 1;
			}

			if (board[j][i] == o) {
				oCount += 1;
			}
		}
		if (xCount == 3) {
			return 0;
		}

		if (oCount == 3) {
			return 0;
		}

		xCount = 0;
		oCount = 0;
	}

	for (int i = 0; i < rows; ++i) {
		if (board[i][i] == x) {
			xCount += 1;
		}

		if (board[i][i] == o) {
			oCount += 1;
		}
	}

	if (xCount == 3) {
		return 0;
	}

	if (oCount == 3) {
		return 0;
	}

	xCount = 0;
	oCount = 0;

	for (int i = 0; i < rows; ++i) {
		if (board[i][rows - 1 - i] == x) {
			xCount += 1;
		}

		if (board[i][rows - 1 - i] == o) {
			oCount += 1;
		}
	}

	if (xCount == 3) {
		return 0;
	}

	if (oCount == 3) {
		return 0;
	}

	return 1;
}

int checkTie(char board[rows][cols]) {
	size_t boardSize = rows * cols;
	int nonClearSpaces = 0;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			if (board[i][j] != '+') {
				nonClearSpaces += 1;
			}
		}
	}

	if (nonClearSpaces == boardSize) {
		return 0;
	}

	return 1;
}


void placeMarker(char board[rows][cols], char user) {

	while(1) {
		int userRow, userCol;

		printf("Enter Row: ");
		scanf("%d", &userRow);

		printf("Enter Column: ");
		scanf("%d", &userCol);

		if (board[userRow - 1][userCol - 1] != '+') {
			printf("Space already chosen, choose another.\n");
			continue;
		}
	board[userRow - 1][userCol - 1] = user;
	break;
	}
}

int main() {
	char board[3][3] = {
		{"+++"},
		{"+++"},
		{"+++"}
	};

	char user1 = 'X';
	char user2 = 'O';

	int userRow;
	int userCol;

	while (1) {
		printBoard(board);

		printf("X's Turn\n");
		placeMarker(board, user1);
		printBoard(board);

		if (checkWin(board) == 0) {
			printf("X WINS!\n");
			break;
		}

		if (checkTie(board) == 0) {
			printf("TIE!\n");
			break;
		}

		printf("O's Turn\n");
		placeMarker(board, user2);

		if (checkWin(board) == 0) {
			printBoard(board);
			printf("O WINS!\n");
			break;
		}
	}

	return 0;
}
